package sstable

import (
	"bufio"
	"encoding/binary"
	"errors"
	"gitlab.com/tswinson/varuint"
	"os"
	"strconv"
)

const (
	maxKeysInMemory   = 1000000 // TODO analysis needed to find optimal value. Or maybe it should be configurable.
	minIntervalLength = 100
)

// An SSTable holds key/value pairs on disk and supports retrieving values by key.
type SSTable struct {
	indexFile      *os.File
	keyFile        *os.File
	dataFile       *os.File
	lock           *tableLock
	intervalLength uint32
	index          []indexEntry
}

type tableLock struct {
	previousKey string
	nextOffset  uint64
	size        uint32
	indexWriter *bufio.Writer
	keyWriter   *bufio.Writer
	dataWriter  *bufio.Writer
}

type indexEntry struct {
	key    string
	offset int64
}

// Load reconstructs an existing SSTable from disk files
func Load(indexFilename, keyFilename, dataFilename string) (*SSTable, error) {
	indexFile, indexFileError := os.Open(indexFilename)
	if indexFileError != nil {
		return nil, indexFileError
	}

	keysFile, keysFileError := os.Open(keyFilename)
	if keysFileError != nil {
		return nil, keysFileError
	}

	dataFile, dataFileError := os.Open(dataFilename)
	if dataFileError != nil {
		return nil, dataFileError
	}

	table := &SSTable{indexFile, keysFile, dataFile, nil, 0, nil}
	table.lock = &tableLock{"", 0, 0, bufio.NewWriter(table.indexFile), bufio.NewWriter(table.keyFile), bufio.NewWriter(table.dataFile)}
	err := table.loadIndex()
	if err != nil {
		return nil, err
	}
	table.lock = nil

	return table, nil
}

// Create creates a new SSTable, saving to specified files.
// After Create is called, Add should be called until all data is added,
// then Finish should be called.
func Create(indexFilename, keysFilename, dataFilename string) (*SSTable, error) {
	indexFile, indexFileError := os.Create(indexFilename)
	if indexFileError != nil {
		return nil, indexFileError
	}

	keysFile, keysFileError := os.Create(keysFilename)
	if keysFileError != nil {
		return nil, keysFileError
	}

	dataFile, dataFileError := os.Create(dataFilename)
	if dataFileError != nil {
		return nil, dataFileError
	}

	lock := &tableLock{"", 0, 0, bufio.NewWriter(indexFile), bufio.NewWriter(keysFile), bufio.NewWriter(dataFile)}

	table := &SSTable{indexFile, keysFile, dataFile, lock, 0, nil}
	return table, nil
}

// Add appends a record to the SSTable. Key_n must be > Key_{n-1} (add to the table in ascending order).
// Duplicate keys are not allowed.
func (table *SSTable) Add(key string, value []byte, deleted bool) error {
	if table.lock != nil {
		if key == "" {
			return errors.New("empty string key not allowed")
		}

		if table.lock.previousKey == key {
			return errors.New("duplicate key '" + key + "' not allowed")
		}

		if table.lock.previousKey > key {
			return errors.New("key '" + key + "' should be > previous key '" + table.lock.previousKey + "'")
		}

		keyLen := varuint.ToVaruint(uint64(len(key)))
		offset := varuint.ToVaruint(table.lock.nextOffset)
		dataLen := varuint.ToVaruint(uint64(len(value)))
		auxByte := createAuxByte(deleted, uint8(len(offset)), uint8(len(dataLen)))

		table.lock.keyWriter.Write(keyLen)
		table.lock.keyWriter.WriteString(key)
		table.lock.keyWriter.Write([]byte{auxByte})
		table.lock.keyWriter.Write(offset)
		table.lock.keyWriter.Write(dataLen)
		table.lock.dataWriter.Write(value)

		table.lock.nextOffset += uint64(len(value))
		table.lock.previousKey = key
		table.lock.size++
		return nil
	}

	return errors.New("can't add to table after Finish is called")
}

// Finish indicates that no more Adds will be done and that the SSTable is ready for use
// and cannot be changed.
func (table *SSTable) Finish() error {

	err := table.lock.keyWriter.Flush()
	if err != nil {
		return err
	}

	err = table.lock.dataWriter.Flush()
	if err != nil {
		return err
	}

	err = table.createIndexFile()
	if err != nil {
		return err
	}

	err = table.lock.indexWriter.Flush()
	if err != nil {
		return err
	}

	err = table.loadIndex()
	if err != nil {
		return err
	}

	table.lock = nil
	return nil
}

// Close closes the underlying files and prevents additional reads from this sstable.
func (table *SSTable) Close() error {
	err := table.indexFile.Close()
	if err != nil {
		return err
	}
	err = table.keyFile.Close()
	if err != nil {
		return err
	}
	err = table.dataFile.Close()
	if err != nil {
		return err
	}

	table.lock = &tableLock{}
	return nil
}

// Find returns a boolean indicating whether the key was found,
// whether the key was deleted, and the value associated with the key if it was not deleted.
func (table *SSTable) Find(key string) (bool, bool, []byte, error) {
	if table.lock == nil {
		var startingOffset int64
		if len(table.index) > 0 {
			startingOffset = search(table.index, key, 0, len(table.index)-1)
		}
		return table.scan(startingOffset, table.intervalLength, key)
	}

	return false, false, nil, errors.New("can't call Find on a locked table; call Finish to finalize the table creation first")
}

// Scan scans the table starting at the byte offset indicated by startOffet, looking for the key
// until maxNumKeys have been examined. It returns a boolean indicating whether the key was found,
// whether the key was deleted, and the value associated with the key if it was not deleted.
func (table *SSTable) scan(startOffset int64, maxNumKeys uint32, key string) (bool, bool, []byte, error) {
	if table.lock != nil {
		return false, false, nil, errors.New("can't Scan a locked table; call Finish to finalize the table creation first")
	}

	offset := startOffset

	for i := uint32(0); i < maxNumKeys; i++ {
		// TODO create getKeys which returns maxNumKeys - performance enhancement
		currentKey, deleted, nextKeyOffset, valueInfoOffset := getKey(offset, table.keyFile)
		if currentKey == key {
			if deleted {
				return true, true, nil, nil
			}
			valueOffset, valueLen := getValueLocation(valueInfoOffset, table.keyFile)
			value := getValue(valueOffset, valueLen, table.dataFile)
			return true, false, value, nil
		}

		offset = nextKeyOffset
	}

	return false, false, nil, nil
}

func (table *SSTable) createIndexFile() error {
	if table.lock != nil {
		intervalLength := getIntervalLength(table.lock.size)
		newOffset, err := table.indexFile.Seek(0, 0)
		if err != nil {
			return err
		}
		if newOffset != 0 {
			return errors.New("couldn't seek to start of index file")
		}
		numKeysBytes := make([]byte, 4)
		intervalBytes := make([]byte, 4)
		binary.LittleEndian.PutUint32(numKeysBytes, table.lock.size/intervalLength)
		binary.LittleEndian.PutUint32(intervalBytes, intervalLength)
		var n int
		n, err = table.lock.indexWriter.Write(numKeysBytes)
		if err != nil {
			return err
		}
		if n != 4 {
			return errors.New("only wrote " + strconv.Itoa(n) + " bytes of 4 for numKeys")
		}
		n, err = table.lock.indexWriter.Write(intervalBytes)
		if err != nil {
			return err
		}
		if n != 4 {
			return errors.New("only wrote " + strconv.Itoa(n) + " bytes of 4 for intervalLength")
		}

		keysOffset := int64(0)
		for i := uint32(1); i <= table.lock.size; i++ {
			keyStartOffset := keysOffset
			keyLen, numBytesForKeyLen := varuint.DecodeVaruint(table.keyFile, keysOffset)
			keysOffset += int64(numBytesForKeyLen)

			key := make([]byte, keyLen)
			n, err = table.keyFile.ReadAt(key, keysOffset)
			if err != nil {
				return err
			}
			if uint64(n) != keyLen {
				return errors.New("only read " + strconv.Itoa(n) + " bytes of " + strconv.Itoa(int(keyLen)) + " for key")
			}
			keysOffset += int64(keyLen)

			auxByte := make([]byte, 1)
			n, err = table.keyFile.ReadAt(auxByte, keysOffset)
			if err != nil {
				return err
			}
			if uint64(n) != 1 {
				return errors.New("only read " + strconv.Itoa(n) + " bytes of 1 for aux byte")
			}

			keysOffset++
			_, numBytesUntilNextKey := decodeAuxByte(auxByte[0])
			keysOffset += int64(numBytesUntilNextKey)

			if i%intervalLength == 0 {
				keyLenBytes := varuint.ToVaruint(keyLen)
				keyStartOffsetBytes := varuint.ToVaruint(uint64(keyStartOffset))
				n, err = table.lock.indexWriter.Write(keyLenBytes)
				if err != nil {
					return err
				}
				if n != len(keyLenBytes) {
					return errors.New("only wrote " + strconv.Itoa(n) + " bytes of " + strconv.Itoa(len(keyLenBytes)) + " for key length")
				}
				n, err = table.lock.indexWriter.Write(key)
				if err != nil {
					return err
				}
				if n != len(key) {
					return errors.New("only wrote " + strconv.Itoa(n) + " bytes of " + strconv.Itoa(len(key)) + " for key")
				}
				n, err = table.lock.indexWriter.Write(keyStartOffsetBytes)
				if err != nil {
					return err
				}
				if n != len(keyStartOffsetBytes) {
					return errors.New("only wrote " + strconv.Itoa(n) + " bytes of " + strconv.Itoa(len(keyStartOffsetBytes)) + " for key start offset")
				}
			}
		}
		// format: {key_length}{key}{key_location_offset}
		return nil
	}

	return errors.New("can't create index file when table isn't locked")
}

func (table *SSTable) loadIndex() error {
	if table.lock != nil {
		newOffset, err := table.indexFile.Seek(0, 0)
		if err != nil {
			return err
		}
		if newOffset != 0 {
			return errors.New("couldn't seek to start of index file")
		}
		buffer := make([]byte, 8)
		var n int
		n, err = table.indexFile.Read(buffer)
		if err != nil {
			return err
		}
		if n != 8 {
			return errors.New("only read " + strconv.Itoa(n) + " of required 8 bytes from index file for numKeys and intervalLength")
		}
		numKeysBytes := buffer[:4]
		intervalBytes := buffer[4:]
		numKeys := binary.LittleEndian.Uint32(numKeysBytes)
		intervalLength := binary.LittleEndian.Uint32(intervalBytes)
		index := []indexEntry{}
		indexFileOffset := int64(len(numKeysBytes) + len(intervalBytes))

		for i := uint32(0); i < numKeys; i++ {
			keyLen, numBytesInKeyLen := varuint.DecodeVaruint(table.indexFile, indexFileOffset)
			indexFileOffset += int64(numBytesInKeyLen)

			keyBytes := make([]byte, keyLen)
			n, err = table.indexFile.ReadAt(keyBytes, indexFileOffset)
			if err != nil {
				return err
			}
			if uint64(n) != keyLen {
				return errors.New("only read " + strconv.Itoa(n) + " of required " + strconv.Itoa(int(keyLen)) + " bytes from index file for key length")
			}
			key := string(keyBytes)
			indexFileOffset += int64(keyLen)

			keyOffset, numBytesInKeyOffset := varuint.DecodeVaruint(table.indexFile, indexFileOffset)
			indexFileOffset += int64(numBytesInKeyOffset)

			index = append(index, indexEntry{key, int64(keyOffset)})
		}
		table.index = index
		table.intervalLength = intervalLength
		return nil
	}
	return errors.New("can't load index file when table isn't locked")
}

func search(index []indexEntry, key string, i, j int) int64 {
	middlePos := (j-i)/2 + i
	if middlePos >= len(index) {
		return index[j].offset
	}

	middle := index[middlePos]
	if key == middle.key {
		return middle.offset
	} else if key < middle.key {
		if j < i {
			if i == 0 {
				return 0
			}
			return index[j].offset
		}
		return search(index, key, i, middlePos-1)
	} else {
		if i > j {
			return index[j].offset
		}
		return search(index, key, middlePos+1, j)
	}
}

func getIntervalLength(numKeys uint32) uint32 {
	intervalLength := numKeys / maxKeysInMemory
	if minIntervalLength > intervalLength {
		return minIntervalLength
	}
	return intervalLength
}

func createAuxByte(deleted bool, offsetByteLen, dataLenByteLen uint8) byte {
	deletedBit := 0
	if deleted {
		deletedBit = 1 << 7
	}

	bytesTilNextKey := int(offsetByteLen + dataLenByteLen)

	return byte(deletedBit | bytesTilNextKey)
}

// decodeAuxByte returns the deleted status (first bit) and number of bytes until next key (right-most 4 bits)
func decodeAuxByte(auxByte byte) (bool, uint8) {
	deletedBit := auxByte >> 7
	var deleted bool
	deleted = deletedBit == 1
	numBytes := auxByte & 15

	return deleted, numBytes
}

// getKey returns the key at the specified offset in the keyFile
// returns: key, deleted, nextKeyOffset, valueInfoOffset
func getKey(offset int64, keyFile *os.File) (string, bool, int64, int64) {
	keyLen, numBytesForKeyLen := varuint.DecodeVaruint(keyFile, offset)

	buffer := make([]byte, keyLen+1)
	nextOffset := offset + int64(numBytesForKeyLen)
	keyFile.ReadAt(buffer, nextOffset)

	key := buffer[0:keyLen]
	auxByte := buffer[keyLen : keyLen+1]
	deleted, numBytesUntilNextKey := decodeAuxByte(auxByte[0])

	valueInfoOffset := nextOffset + int64(len(key)) + 1
	nextKeyOffset := valueInfoOffset + int64(numBytesUntilNextKey)

	return string(key), deleted, nextKeyOffset, valueInfoOffset
}

// getValueLocation returns the offset and length of the data value
// indicated in the key file at the offset
func getValueLocation(offset int64, keyFile *os.File) (int64, uint32) {
	valueOffset, numBytesForValueOffet := varuint.DecodeVaruint(keyFile, offset)
	valueLen, _ := varuint.DecodeVaruint(keyFile, offset+int64(numBytesForValueOffet))
	return int64(valueOffset), uint32(valueLen)
}

// getValue returns the value at the offset specified in the dataFile
func getValue(offset int64, length uint32, dataFile *os.File) []byte {
	value := make([]byte, length)
	dataFile.ReadAt(value, offset)
	return value
}
