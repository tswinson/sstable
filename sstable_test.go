package sstable

import (
	"bytes"
	"os"
	"strconv"
	"strings"
	"testing"
)

var err error
var table *SSTable

func TestCreate(t *testing.T) {
	table, err = Create("index.test", "keys.test", "data.test")

	if table == nil || err != nil {
		t.Errorf("Couldn't create table. Error: %v", err)
	}
}

func TestAdd(t *testing.T) {
	err = table.Add("a", []byte{65}, false)
	if err != nil {
		t.Errorf("An error occurred adding key: %v", err)
	}

	err = table.Add("a", []byte{1}, false)
	if err == nil {
		t.Errorf("Erroneously added duplicate key")
	}

	err = table.Add("", []byte{1}, false)
	if err == nil {
		t.Errorf("Erroneously added empty string key")
	}

	err = table.Add("d", []byte{68}, false)
	if err != nil {
		t.Errorf("An error occurred adding key: %v", err)
	}

	err = table.Add("c", []byte{67}, false)
	if err == nil {
		t.Error("keys must be added in order")
	}

	for key := 1000; key < 1999; key++ {
		data, deleted := getValueForKey(key)
		err = table.Add("key_"+strconv.Itoa(key), data, deleted)
		if err != nil {
			t.Errorf("An error occurred adding key: %v", err)
		}
	}

	// table now contains keys: a,c,1000,1001,...,1998
	// size = 1000
}

func TestFinish(t *testing.T) {
	err = table.Finish()
	if err != nil {
		t.Errorf("Error calling Finish(): %v", err)
	}
}

func TestFind(t *testing.T) {
	for key := 1000; key < 1999; key++ {
		expectedData, expectedDeleted := getValueForKey(key)
		found, deleted, data, error := table.Find("key_" + strconv.Itoa(key))
		if error != nil {
			t.Errorf("An error occurred retrieving key_%d: %v", key, err)
		}
		if !found {
			t.Errorf("Couldn't find key_%d", key)
		}
		if deleted != expectedDeleted {
			t.Errorf("key_%d erroneously has deleted=%t", key, deleted)
		}
		if !deleted && !bytes.Equal(data, expectedData) {
			t.Errorf("data bytes retrieved for key_%d are incorrect!", key)
		} else if deleted && data != nil {
			t.Errorf("got data bytes for deleted key_%d", key)
		}
	}

	found, _, _, _ := table.Find("z")
	if found {
		t.Errorf("found non-existant key 'z'")
	}
}

func TestLoad(t *testing.T) {
	table2, error := Load("index.test", "keys.test", "data.test")
	if error != nil {
		t.Errorf("couldn't load table: %v", error)
	}

	found, deleted, data, err2 := table2.Find("key_1001")
	if err2 != nil {
		t.Errorf("couldn't find key_1001 in loaded table: %v", err2)
	}
	expectedData, expectedDeleted := getValueForKey(1001)
	if !found {
		t.Errorf("key_1001 not found in loaded table!")
	}
	if !bytes.Equal(data, expectedData) {
		t.Errorf("bad data for key_1001")
	}
	if deleted != expectedDeleted {
		t.Errorf("key_1001 was deleted!")
	}

	found, deleted, data, _ = table2.Find("a")
	if !found {
		t.Errorf("key 'a' not found in loaded table!")
	}
	if !bytes.Equal(data, []byte{65}) {
		t.Errorf("bad data for key 'a'")
	}
	if deleted {
		t.Errorf("key 'a' was deleted!")
	}
	table2.Close()
}

func getValueForKey(key int) ([]byte, bool) {
	var str strings.Builder

	for i := 0; i < key; i++ {
		str.WriteString("-" + string(i))
	}

	return []byte(str.String() + "|"), key%2 == 0
}

func TestClose(t *testing.T) {
	table.Close()
	_, _, _, err = table.Find("a")
	if err == nil {
		t.Error("shouldn't be able to read from a closed table")
	}
	err = os.Remove("data.test")
	if err != nil {
		t.Errorf("error during teardown: %v", err)
	}
	err = os.Remove("keys.test")
	if err != nil {
		t.Errorf("error during teardown: %v", err)
	}
	err = os.Remove("index.test")
	if err != nil {
		t.Errorf("error during teardown: %v", err)
	}
}
